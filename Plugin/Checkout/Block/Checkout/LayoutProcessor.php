<?php 
namespace Bleez\Correios\Plugin\Checkout\Block\Checkout;
use Magento\Checkout\Block\Checkout\LayoutProcessor as MageLayoutProcessor;

class LayoutProcessor
{

/**
 * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
 * @param array $jsLayout
 * @return array
 */
/*public function afterProcess(
    \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
    array  $jsLayout
) {
   
  $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
               ['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']
               ['children']['postcode']['component'] = 'Bleez_Correios/js/form/element/post-code';

   $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
               ['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']
               ['children']['postcode']['config']['elementTmpl'] = 'Bleez_Correios/form/element/post-code';

   

    return $jsLayout;
    }
}*/

public function afterProcess(MageLayoutProcessor $subject, $jsLayout)
    {

        /*config: checkout/options/display_billing_address_on = payment_method */
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
            ['payment']['children']['payments-list']['children']
        )) {

            foreach ($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                     ['payment']['children']['payments-list']['children'] as $key => $payment) {

                /* company */
               /* if (isset($payment['children']['form-fields']['children']['company'])) {

                    $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                    ['payment']['children']['payments-list']['children'][$key]['children']['form-fields']['children']
                    ['company']['sortOrder'] = 0;
                }*/

                $method = substr($key, 0, -5);
            
                $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
               ['payment']['children']['payments-list']['children'][$key]['children']['form-fields']
               ['children']['postcode']['component'] = 'Bleez_Correios/js/form/element/post-code';

               $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
               ['payment']['children']['payments-list']['children'][$key]['children']['form-fields']
               ['children']['postcode']['config']['elementTmpl'] = 'Bleez_Correios/form/element/post-code';
            
               $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
               ['payment']['children']['payments-list']['children'][$key]['children']['form-fields']
               ['children']['telephone']['config']['elementTmpl'] = 'Magento_Checkout/form/element/telefone';
               
                /* vat_id */
                $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                ['payment']['children']['payments-list']['children'][$key]['children']['form-fields']['children']
                ['vat_id'] = [
                    'component' => 'Magento_Ui/js/form/element/abstract',
                    'config' => [
                        'customScope' => 'billingAddress' . $method,
                        'customEntry' => null,
                        'elementTmpl' => 'Magento_Checkout/form/element/cpf',
                        'template' => 'ui/form/field',
                        //'template' => 'Magento_Checkout/form/element/cpf',
                        //'elementTmpl' => 'ui/form/element/input',
                        'tooltip' => null,
                    ],
                    'dataScope' => 'billingAddress' . $method . '.vat_id',
                    'dataScopePrefix' => 'billingAddress' . $method,
                    'label' => __('CPF'),
                    'provider' => 'checkoutProvider',
                    'sortOrder' => 1,
                    'validation' => [],
                    'options' => [],
                    'filterBy' => null,
                    'customEntry' => null,
                    'visible' => true,
                ];
            }
        }

        /* config: checkout/options/display_billing_address_on = payment_page */
        if (isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
            ['payment']['children']['afterMethods']['children']['billing-address-form']
        )) {

            if (isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
               ['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']
               ['children']['postcode'])) {
            
                $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
               ['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']
               ['children']['postcode']['sortOrder'] = 1;

               $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
               ['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']
               ['children']['postcode']['component'] = 'Bleez_Correios/js/form/element/post-code';

               $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
               ['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']
               ['children']['postcode']['config']['elementTmpl'] = 'Bleez_Correios/form/element/post-code';
            }

            $method = 'shared';
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
               ['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']
               ['children']['telephone']['config']['elementTmpl'] = 'Magento_Checkout/form/element/telefone';
        
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
               ['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']
               ['children']['postcode']['config']['sortOrder'] = 1;
             $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
               ['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']
               ['children']['telephone']['config']['sortOrder'] = 100;
        
            
            /* vat_id */
            $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
            ['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']
            ['children']['vat_id'] = [
                'component' => 'Magento_Ui/js/form/element/abstract',
                'config' => [
                    'customScope' => 'billingAddress' . $method,
                    'customEntry' => null,
                    'template' => 'ui/form/field',
                    'elementTmpl' => 'Magento_Checkout/form/element/cpf',
                    'tooltip' => null,
                ],
                'dataScope' => 'billingAddress' . $method . '.vat_id',
                'dataScopePrefix' => 'billingAddress' . $method,
                'label' => __('CPF'),
                'provider' => 'checkoutProvider',
                'sortOrder' => 1,
                'validation' => [],
                'options' => [],
                'filterBy' => null,
                'customEntry' => null,
                'visible' => true,
            ];
        }

        return $jsLayout;
    }
}